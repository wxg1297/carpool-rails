# -*- coding: utf-8 -*-
require 'date'
class Api::CarpoolsController < ApplicationController
  # protect_from_forgery with: :null_session, if: Proc.new { |c| c.request.format == 'application/json' }

  skip_before_filter :verify_authenticity_token,
                     :if => Proc.new { |c| c.request.format == 'application/json' }

  before_filter :fetch_user, :except => [:index, :destroy]

  def fetch_user
    @user = User.where(authentication_token: params[:auth_token]).first

  end

  def index
	  @user = User.where(authentication_token: params[:auth_token]).first
	  logger.info("User-input: #{params[:auth_token]}")
	  t = User.where(authentication_token: params[:auth_token]).first
	  logger.info("User-input: #{t}")
      @carpools = Carpool.where(:unit_id => @user.unit_id)
	  
	  @data = []
	  
	  @carpools.each do |c|
		  poster = User.where(:id => c.create_user_id).first
		  logger.info("User-input: #{poster}")
		  @data << { :id =>c.id, :start_location => c.start_location, :end_location => c.end_location, :poster => poster.name, :poster_id => c.create_user_id, :unit_id => c.unit_id, :max_people => c.max_people, :current_user => c.current_user, :start_time => c.start_time.strftime("%Y-%m-%d %H:%M:%S")}
	  end

	  
    respond_to do |format|
		format.json { render json: { success: true, info: "Saved", data: @data }, status: :created }
    end
  end
	def create

    unless @user
      respond_to do |format|
        format.json { render json: { success: false, info: "Expired Session. Please sign in again." }}
      end
    else
		@unit= Unit.where(id: @user.unit_id).first
        @carpool= Carpool.new
	    @carpool.start_location = params[:carpool][:start]
		@carpool.end_location = params[:carpool][:end]
		@carpool.max_people = params[:carpool][:max_person]
		year = Time.now
		hour = params[:carpool][:hour]
		minute = params[:carpool][:minute]
		n = Time.now
		if Time.now > DateTime.new(n.year,n.month,n.day, hour,minute,00)
			@carpool.start_time = DateTime.new(n.year,n.month,n.day+1, hour,minute,00)
		else
			@carpool.start_time = DateTime.new(n.year,n.month,n.day, hour,minute,00)
			
		end
        @carpool.create_user_id = @user.id
		@carpool.unit_id = @unit.id
		
      respond_to do |format|
        if @carpool.save
			@carpools = Carpool.all
          Pusher.trigger( @user.email.to_s , 'add_carpool_event', {
            id: @carpool.id,
            create_user: @user.name
          })

          format.json { render json: { success: true, info: "Saved"}, status: :created }
        else
          format.json { render json: @unit.errors, status: :unprocessable_entity }
        end
      end

    end
  end
end




=begin
	  
=end
=begin
  def edit
	  
	  unless @user
      respond_to do |format|
        format.json { render json: { success: false, info: "Expired Session. Please sign in again." }}
      end
    else
      u = @user
	  u.unit_id = params[:unit][:unit_id]

      respond_to do |format|
        if u.save

          Pusher.trigger( @user.email.to_s , 'add_unit_fron_user_event', {
            id: @user.id,
            unit: u.unit.name
          })

          format.json { render json: { success: true, info: "Saved"}, status: :created }
        else
          format.json { render json: @user.errors, status: :unprocessable_entity }
        end
      end

    end
	  
	  
  end

def renew
	@carpools.each do |carpool|
		#만약 현재 시간 이전에 출발 시간이 끝나면 db를 destroy한다.
		if Time.now > carpool.start_time
			carpool.destroy
		else
			cnt_people = User.where(carpool_id: carpool.id).count
			carpool.current_user = cnt_user
			carpool.save
		end
		
	end
end

  
  def destroy
    @carpool = Unit.find(params[:unit][:id])

    Pusher.trigger( @user.email.to_s , 'delete_carpool_event', {
      id: @carpool.id
    })

    respond_to do |format|
      if @carpool.destroy
        format.json { render json: { success: true, info: "Deleted" } }
      else
        format.json { render json: @unit.errors, status: :unprocessable_entity }
      end
    end
  end
=end

=begin
def apply

	unless @user
		respond_to do |format |
			format.json {
				render json: {
					success: false,
					info: "Expired Session. Please sign in again."
				}
			}
		end
	else
		current_user = User.where(carpool_id: params[:carpool][:carpool_id]).count
		max_people = Carpool.where(id: params[:carpool][:carpool_id]).first.max_people

		if (max_people <= current_people + 1)
			respond_to do |format |
				format.json {
					render json: {
						success: false,
						info: "over max people"
					}
				}
			end
		else

			@user.carpool_id = params[:carpool][:carpool_id]
			respond_to do |format |
				if @user.save

					Pusher.trigger(@user.email.to_s, 'apply_carpool_event', {
						carpool_id: @user.carpool,
						apply_user: @user.name,
						current_user: current_user
					})
					format.json {render json: {success: true, info: "Saved", data: {carpools: @carpools}}, status: :created}
				else
					format.json {render json: @unit.errors, status: :unprocessable_entity}
				end
			end
			@carpools.each do |carpool|
				if Time.now > carpool.start_time
					carpool.destroy
				else
					cnt_user = User.where(carpool_id: carpool.id).count
					carpool.current_user = cnt_user
					carpool.save
				end
			end
		end
	end
end
=end