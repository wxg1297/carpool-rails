# -*- coding: utf-8 -*-
class Api::V1::RegistrationsController < Devise::RegistrationsController
  skip_before_filter :verify_authenticity_token,
                     :if => Proc.new { |c| c.request.format == 'application/json' }

  respond_to :json

  def create
    build_resource(sign_up_params)
    #resource.skip_confirmation!
    if resource.save
      sign_in(resource, :store => false)
      render :status => 200,
             :json => { :success => true,
                        :info => t("devise.registrations.signed_up"),
                        :data => { :user => resource,
                                   :auth_token => current_user.authentication_token.encode("utf-8") } }
    else
      render :status => :unprocessable_entity,
             :json => { :success => false,
                        :info => resource.errors.full_messages,
                        :data => {} }
    end
  end

  def sign_up_params
    params.require(:user).permit(:email,:name,:group,:rank,:phone,:using_car, :password, :password_confirmation)
  end

end