

class Api::UnitsController < ApplicationController
  protect_from_forgery with: :null_session, if: Proc.new { |c| c.request.format == 'application/json' }
	
    # de_name = CGI::escape(query)
	
  skip_before_action :verify_authenticity_token,
                     :if => Proc.new { |c| c.request.format == 'application/json' }

  # http_basic_authenticate_with :name => "myfinance", :password => "credit123"

  #skip_before_action :authenticate_user! # we do not need devise authentication here
  before_action :fetch_user, :except => [:index, :destroy]

  def fetch_user
    @user = User.where(authentication_token: params[:auth_token]).first
	  
  end

  def index
    @units = Unit.all

    respond_to do |format|
      format.json { render json: { success: true, info: "Saved", data: @units } }
    end
  end
	
	def show
		@user = User.where(authentication_token: params[:auth_token]).first
		
    @units = Unit.where(:top_unit => @user.group)
		logger.info("User-input: #{@units.first}")
#jsonArray로 보내기
    respond_to do |format|
      format.json { render json: { success: true, info: "Saved", data: @units } }
    end
  end

  def create
	  @user = User.where(authentication_token: params[:auth_token]).first
logger.info("User-input: #{params[:auth_token]}")
    unless @user
      respond_to do |format|
        format.json { render json: { success: false, info: "Expired Session. Please sign in again." }}
      end
    else
      @unit = Unit.new
	  @unit.top_unit = params[:unit][:top_unit]
	  @unit.unit_name = params[:unit][:unit_name]
      @unit.user_id = @user.id

      respond_to do |format|
        if @unit.save
			@user.unit_id = @unit.id
			if @user.save
				format.json { render json: { success: true, info: "Saved", :data => { :unit_id =>@unit.id, :top_unit => @unit.top_unit, :unit_name => @unit.unit_name} }, status: :created }
			else
				format.json { render json: @user.errors, info: "not Saved" ,status: :unprocessable_entity }
			end
			
           Pusher.trigger( @user.email.to_s , 'add_unit_event', {id: @unit.id,unit_name: @unit.unit_name})

          
        else
          format.json { render json: @unit.errors, status: :unprocessable_entity }
        end
      end

    end
  end

  def destroy

    @unit = Unit.find(params[:unit][:id])

    Pusher.trigger( @unit.user.email.to_s , 'delete_unit_event', {id: @unit.id})

    respond_to do |format|
      if @unit.destroy
        format.json { render json: { success: true, info: "Deleted" } }
      else
        format.json { render json: @unit.errors, status: :unprocessable_entity }
      end
    end
  end
end