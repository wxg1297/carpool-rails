class AddCurrentUserToCarpool < ActiveRecord::Migration[5.0]
  def change
    add_column :carpools, :current_user, :integer, :default => 0
  end
end
