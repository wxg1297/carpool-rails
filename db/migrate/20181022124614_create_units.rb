class CreateUnits < ActiveRecord::Migration[5.0]
  def change
    create_table :units do |t|
		t.text :top_unit
      t.text :unit_name
      t.integer :user_id

      t.timestamps
    end
  end
end