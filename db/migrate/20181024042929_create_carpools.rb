class CreateCarpools < ActiveRecord::Migration[5.0]
  def change
    create_table :carpools do |t|
      t.integer :create_user_id
      t.text :start_location
      t.text :end_location
      t.integer :max_people
      t.datetime :start_time
      t.references :unit, foreign_key: true

      t.timestamps
    end
  end
end
