class AddCarpoolToUser < ActiveRecord::Migration[5.0]
  def change
    add_reference :users, :carpool, foreign_key: true
  end
end
