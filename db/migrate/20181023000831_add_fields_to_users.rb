class AddFieldsToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :name, :text
    add_column :users, :group, :text
    add_column :users, :rank, :text
    add_column :users, :phone, :string
    add_column :users, :using_car, :boolean
    add_reference :users, :unit, foreign_key: true
  end
end
