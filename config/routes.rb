Rails.application.routes.draw do
  get 'home/index'

  devise_for :users
	
  resources :carpools
	resources :units


  namespace :api do
    post 'carpools/delete' => 'carpools#destroy' , :defaults => { :format => 'json' }
	  post 'carpools/edit' => 'carpools#edit' , :defaults => { :format => 'json' }
	  post 'carpools/index' => 'carpools#index' , :defaults => { :format => 'json' }
	  post 'carpools/create' => 'carpools#create' , :defaults => { :format => 'json' }
    resources :carpools, :only => [:index, :create], :defaults => { :format => 'json' }
	  post 'units/delete' => 'units#destroy' , :defaults => { :format => 'json' }
	  post 'units/show' => 'units#show' , :defaults => { :format => 'json' }
	  post 'units/create' => 'units#create' , :defaults => { :format => 'json' }
	  resources :units, :only => [:index, :create], :defaults => { :format => 'json' }
  end
  
  namespace :api do
    namespace :v1 do
      devise_scope :user do
        post 'registrations' => 'registrations#create', :as => 'register'
        post 'sessions' => 'sessions#create', :as => 'login'
        delete 'sessions' => 'sessions#destroy', :as => 'logout'
      end
    end
  end
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
